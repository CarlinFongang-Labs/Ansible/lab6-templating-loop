# Ansible | Templating-loop

_______

**Prénom** : Carlin

**Nom** : FONGANG

**Email** : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Nous souhaitons préparer un environnement de déploiement, rédiger le playbook nécessaire et valider son exécution, garantissant ainsi que le serveur web Apache fonctionne correctement et soit accessible de l'extérieur sur le port 80. Ce laboratoire nous permettra d'automatiser la tâche de déploiement du serveur Apache avec Ansible, tout en améliorant l'efficacité et la fiabilité des déploiements.

## Objectifs

Dans ce lab, nous allons :

- Créer deux instances : une hôte hébergeant Ansible et une instance cliente.

- Créer un dossier `templates` contenant un fichier `index.html.j2`, qui permettra d'afficher sur le site "Bienvenue sur -nom du client-".

- Nous modifierons le playbook `deploy.yml` afin :

  - D'utiliser **ansible.builtin.apt** (au lieu de "absible.builtin.apt") pour installer git et wget à l'aide du module apt (au lieu de yum, car yum est utilisé pour les systèmes CentOS).

  - D'utiliser une condition afin d'installer wget et git uniquement si nous sommes sur un système CentOS.

  - De modifier le site internet par défaut en copiant un template `index.html.j2` qui devra afficher "Bienvenue sur -nom du client-", le nom du client étant récupéré dans les variables fournies par Ansible.

  - De modifier le déploiement du conteneur Apache pour qu'il monte le fichier `index.html` dans le répertoire où il lit le site internet par défaut (consulter la documentation d'Apache sur Docker Hub). Nous utiliserons la notion de montage de volume.

- Nous vérifierons enfin que le site est bien conforme.

## Prérequis
Disposer de deux machines avec Ubuntu déjà installées.

Dans notre cas, nous allons provisionner des instances EC2 s'exécutant sous Ubuntu via AWS, sur lesquelles nous effectuerons toutes nos configurations.

Documentation complémentaire :

[Documentation Ansible](https://docs.ansible.com/ansible/latest/index.html)

[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

## 1. Création des instances EC2
[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

## 2. Création du fichier index.html.j2 

```bash
cd project
mkdir templates
nano templates/index.html.j2
```

Contenu du fichier jinja :

```bash
Bienvenu sur {{ ansible_hostname }}
```

## 3. Modification du playbook deploy.yml

Nous allons ajouter **git** et **wget** dans la liste des paquets à installer dans le playbook à l'étape des pre_tasks :

```bash
pre_tasks:
    - name: Install some packages
      ansible.builtin.apt:
        name:
          - python3-pip
          - apt-transport-https
          - ca-certificates
          - curl
          - gnupg-agent
          - software-properties-common
          - wget
          - git 
        state: present
        update_cache: true
      when: ansible_os_family == "Debian"
```

Ajout de volume dans notre déploiement Docker :

```bash
  tasks:
    - name: Copy website file template
      ansible.builtin.template:
        src: index.html.j2
        dest: "/home/{{ ansible_env.SUDO_USER | default(ansible_env.USER) }}/index.html"
        mode: '0644'
    - name: Create Apache container
      community.docker.docker_container:
        name: apache_aCD
        image: httpd
        ports:
          - "80:80"
        volumes:
          - "/home/{{ ansible_env.SUDO_USER | default(ansible_env.USER) }}/index.html:/usr/local/apache2/htdocs/index.html"
```

Une fois les modifications apportées au playbook, nous allons l'exécuter et vérifier que le site fonctionne.

## 4. Déploiement du conteneur et test du site

```bash
ansible-playbook -i hosts.yml deploy.yml
```

>![alt text](img/image.png)
*Sortie console de l'exécution du playbook*

Vérification de la mise en ligne du site :

>![alt text](img/image-1.png)

*Le site a bien chargé et a été modifié pour afficher le nom du client sur lequel il est hébergé.*

En se connectant à la machine cliente et en inspectant les fichiers, on peut voir que le fichier **index.html** a bien été copié et le conteneur httpd a bien été lancé :

>![alt text](img/image-2.png)

Arborescence finale du projet :
>![alt text](img/image-3.png)
